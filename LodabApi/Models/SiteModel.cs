﻿using System.Collections.Generic;

namespace LodabApi.Models
{
	public class SiteModel : SuspendableModel
    {
		public string Owner { get; set; }
		public int PhoneNumber { get; set; }
		public List<MenuItemModel> MenuItems { get; set; }

	    public SiteModel()
	    {
		    SiteId = 0;
	    }
    }
}
