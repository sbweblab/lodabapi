﻿using System.Collections.Generic;

namespace LodabApi.Models
{
	public class ItemModel : BaseModel
	{
		public string Title { get; set; }
		public string DisplayName { get; set; }
		public string Template { get; set; }
		public ImageModel MainImage { get; set; }
		public List<ImageModel> SubImages { get; set; }
		public List<DetailModel> Details { get; set; }
		public string Description { get; set; }
		public int Order { get; set; }
		public bool IsDefault { get; set; }

		public ItemModel()
		{
			SubImages = new List<ImageModel>();
			Details = new List<DetailModel>();
			IsDefault = false;
			Order = 0;
		}
	}
}
