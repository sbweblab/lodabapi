﻿namespace LodabApi.Models
{
	public class ImageModel : BaseModel
	{
		public int Order { get; set; }
		public string FileName { get; set; }
	}
}
