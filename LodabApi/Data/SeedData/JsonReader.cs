﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace LodabApi.Data.SeedData
{
	public class JsonReader<TModel>
    {
	    private List<TModel> _modelList;

	    public JsonReader()
	    {
		    _modelList = new List<TModel>();
	    }
	    public List<TModel> GetJsonFile(string fileName)
	    {
			using (StreamReader r = new StreamReader(fileName))
			{
				string json = r.ReadToEnd();
				_modelList = JsonConvert.DeserializeObject<List<TModel>>(json);
			}
		    return _modelList;
	    }
    }
}
