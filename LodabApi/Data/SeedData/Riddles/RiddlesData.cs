﻿using System.IO;
using LodabApi.Models.RiddleModels;

namespace LodabApi.Data.SeedData.Riddles
{
	public class RiddlesData
	{
		private readonly ApiContext _context;

		public RiddlesData(ApiContext context)
		{
			_context = context;
		}

		public void SeedData()
		{
			var models = new JsonReader<RiddleModel>();
			var path = $"{Directory.GetCurrentDirectory()}\\Data\\SeedData\\Riddles\\Riddles.json";
			_context.Riddle.AddRange(models.GetJsonFile(path));
			_context.SaveChanges();
		}
	}
}
