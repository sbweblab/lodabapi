﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LodabApi.Models
{
	public enum TemplateType {
		Profile, 
		MiniProfile,
		NavContent
	}

	public class PageModel : SuspendableModel
	{
		public string Template { get; set; }
		//public string ContentTarget { get; set; }
		public List<GroupModel> Groups { get; set; }
	}
}
