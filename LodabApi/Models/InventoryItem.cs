﻿using System;
using System.Collections.Generic;

namespace LodabApi.Models
{
	namespace LodabApi.Models
	{
		public class InventoryItem : SuspendableModel
		{
			public string Title { get; set; }
			public string Name { get; set; }
			public string MenuName { get; set; }
			public ImageModel MainImage { get; set; }
			public List<ImageModel> SubImages { get; set; }
			public List<DetailModel> Details { get; set; }
			public string Description { get; set; }
			public float Price { get; set; }
			public float OriginalPrice { get; set; }
			public float SalePrice { get; set; }
			public DateTime StartTime { get; set; }
			public DateTime EndTime { get; set; }
			public bool IsDefault { get; set; }

			public InventoryItem()
			{
				SubImages = new List<ImageModel>();
				Details = new List<DetailModel>();
			}
		}
	}

}
