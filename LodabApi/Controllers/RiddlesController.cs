﻿using LodabApi.Data;
using LodabApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace LodabApi.Controllers
{
	[Route("[controller]")]
    public class RiddlesController : Controller
    {
	    private readonly ApiContext _context;
	    private readonly IApiService _service;

	    public RiddlesController(ApiContext context, IServiceProvider provider) 
	    {
			_service = provider.GetRequiredService<RiddleService>();
			_context = context;
		}

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] string page = "1", [FromQuery] string count = "10")
        {
	        return Json(await _service.GetCollection(page, count));
			}
		
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
	        return Json(await _service.GetItem(id));
	        //return Json(_context.Riddle.FirstOrDefault(x => x.Id == id));
        }
		
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]RiddleModel value)
        {
	        if (!ModelState.IsValid) return BadRequest(ModelState);

	        var x = await _service.PutItem(id, value);

	        return Ok(x);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
