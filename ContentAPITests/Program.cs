﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RiddlesAPITests.RiddlesSvc;
namespace RiddlesAPITests
{
    public enum ExitCode
    {
        Success = 0,
        TestCreateRiddleFail = 3,
        TestCreateRiddleMissingRequiredDataFail = 4,
        TestCreateRiddleBadDataFail = 5,
        TestGetRiddlesFail = 6,
        TestGetRiddleFail = 7,
        TestGetRiddleIdNotFoundFail = 8,
        TestUpdateRiddleFail = 9,
        TestUpdateRiddleBadIdFail = 10,
        TestUpdateRiddleIdNotFoundFail = 11,
        TestDeleteRiddleFail = 12,
        TestDeleteRiddleBadIdFail = 13,
        TestDeleteRiddleIdNotFoundFail = 14,
        TestUpdateRiddleNoChangesFail = 15
    }
    
    public class Program
    {
        public const string BaseUrl = "http://localhost:51921/api";
        public static int TestRiddleId = 0;
        public static int RiddleCount = 0;
        public static JsonSerializerSettings MicrosoftDateFormatSettings;

        public static int Main(string[] args)
        {
            MicrosoftDateFormatSettings = new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
            if (Test_CreateRiddle() == false) Exit(ExitCode.TestCreateRiddleFail);
            if (Test_CreateRiddle_MissingRequiredData() == false) Exit(ExitCode.TestCreateRiddleMissingRequiredDataFail);
            if (Test_CreateRiddle_BadData() == false) Exit(ExitCode.TestCreateRiddleBadDataFail);
            if (Test_GetRiddles() == false) Exit(ExitCode.TestGetRiddlesFail);
            if (Test_GetRiddle() == false) Exit(ExitCode.TestGetRiddleFail);
            if (Test_GetRiddle_IdNotFound() == false) Exit(ExitCode.TestGetRiddleIdNotFoundFail);
            if (Test_UpdateRiddle() == false) Exit(ExitCode.TestUpdateRiddleFail);
            if (Test_UpdateRiddle_NoChanges() == false) Exit( ExitCode.TestUpdateRiddleNoChangesFail);
            if (Test_UpdateRiddle_BadId() == false) Exit(ExitCode.TestUpdateRiddleBadIdFail);
            if (Test_UpdateRiddle_IdNotFound() == false) Exit(ExitCode.TestUpdateRiddleIdNotFoundFail);
            if (Test_DeleteRiddle_BadId() == false) Exit(ExitCode.TestDeleteRiddleBadIdFail);
            if (Test_DeleteRiddle_IdNotFound() == false) Exit(ExitCode.TestDeleteRiddleIdNotFoundFail);
            if (Test_DeleteRiddle() == false) Exit(ExitCode.TestDeleteRiddleFail);
            Debug.WriteLine(ExitCode.Success);
            return (int)ExitCode.Success;
        }
        public static void Exit(ExitCode exitCode)
        {
            Debug.WriteLine("Exited with code " + (int)exitCode);
            Environment.Exit((int) exitCode);
        }

        public static bool Test_CreateRiddle()
        {
            try
            {
                const string url = BaseUrl + @"/riddles";
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var riddle = new Riddle
                {
                    Question = @"How many licks does it take to get to the tootsie roll center of a tootsie pop?",
                    Answer = @"The world may never know.",
                    Hint = @"Lets find out, one, a-ta-whoo, a-thrrree. A-Three.",
                    HonorableMention = @"Please kill that damned owl.",
                    IsActive = true
                };
                var json = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = httpClient.PostAsync(url, content);
                var result = response.Result.Content.ReadAsStringAsync();
                var apiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                if (apiResponse.StatusCode != (int) HttpStatusCode.Created) return false;
                var riddleResponse = JsonConvert.DeserializeObject<Riddle>(apiResponse.Data.ToString());
                TestRiddleId = riddleResponse.Id;
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_CreateRiddle_MissingRequiredData()
        {
            try
            {
                const string url = BaseUrl + @"/riddles";
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var riddle = new Riddle
                {
                    Question = @"",
                    Answer = @"",
                    Hint = @"",
                    HonorableMention = @"",
                    IsActive = true
                };
                var json = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = httpClient.PostAsync(url, content);
                var result = response.Result.Content.ReadAsStringAsync();
                var apiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                return apiResponse.StatusCode == (int) HttpStatusCode.BadRequest;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_CreateRiddle_BadData()
        {
            try
            {
                const string url = BaseUrl + @"/riddles";
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var guid = new Guid();
                var json = JsonConvert.SerializeObject(guid, MicrosoftDateFormatSettings);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = httpClient.PostAsync(url, content);
                response.Result.Content.ReadAsStringAsync();
                return response.Result.StatusCode == HttpStatusCode.BadRequest;
            }
            catch (Exception e)
            {
                if (e is JsonReaderException) 
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_GetRiddles()
        {
            try
            {
                const string url = BaseUrl + @"/riddles";
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var dbRiddles = JsonConvert.DeserializeObject<List<Riddle>>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                RiddleCount = dbRiddles.Count;
                return dbRiddles.Count > 0; /* if records are present, this test passes */
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_GetRiddle()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId; // use riddle id from previous create test
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var riddle = JsonConvert.DeserializeObject<Riddle>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                if (string.IsNullOrWhiteSpace(riddle.Question)) return false; // if riddle has no question we have an empty object
                return getApiResponse.StatusCode == (int) HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_GetRiddle_IdNotFound()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + (RiddleCount + 10); // use a riddle id above the count of the records
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                return getApiResponse.StatusCode == (int)HttpStatusCode.NotFound; // we expect a 404 Not Found here
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_UpdateRiddle()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId; // get the riddle
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var riddle = JsonConvert.DeserializeObject<Riddle>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                var oldQuestion = riddle.Question; // store the old question
                riddle.Question =
                    @"How many licks does it take to get to the tootsie roll center of a tootsie pop? Let's find out.";
                var ts = DateTime.Now;
                riddle.Modified = ts;
                var dbRiddleJson = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings); // re-serialize riddle and update the db object
                var content = new StringContent(dbRiddleJson, Encoding.UTF8, "application/json");
                var putResponse = httpClient.PutAsync(url, content);
                var result = putResponse.Result.Content.ReadAsStringAsync();
                var putApiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                if (putApiResponse.StatusCode != (int) HttpStatusCode.OK) return false;

                riddle.Question = oldQuestion; // reset data
                dbRiddleJson = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings);
                content = new StringContent(dbRiddleJson, Encoding.UTF8, "application/json");
                putResponse = httpClient.PutAsync(url, content);
                result = putResponse.Result.Content.ReadAsStringAsync();
                putApiResponse = JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                return putApiResponse.StatusCode == (int) HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_UpdateRiddle_NoChanges()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId; // get the riddle
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var riddle = JsonConvert.DeserializeObject<Riddle>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                var dbRiddleJson = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings); // re-serialize riddle and update the db object, no changes were made
                var content = new StringContent(dbRiddleJson, Encoding.UTF8, "application/json");
                var putResponse = httpClient.PutAsync(url, content);
                var result = putResponse.Result.Content.ReadAsStringAsync();
                var putApiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                return putApiResponse.StatusCode == (int) HttpStatusCode.OK &&
                       putApiResponse.StatusMessage == @"No changes.";
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_UpdateRiddle_BadId()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId; // get the riddle
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var riddle = JsonConvert.DeserializeObject<Riddle>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                url = BaseUrl + @"/riddles/crap"; // change the URL to crap
                var dbRiddleJson = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings); // re-serialize riddle and update the db object, no changes were made
                var content = new StringContent(dbRiddleJson, Encoding.UTF8, "application/json");
                var putResponse = httpClient.PutAsync(url, content);
                var result = putResponse.Result.Content.ReadAsStringAsync();
                var putApiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                return putApiResponse.StatusCode == (int) HttpStatusCode.BadRequest;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_UpdateRiddle_IdNotFound()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId; // get the riddle
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.GetAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var getApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                var riddle = JsonConvert.DeserializeObject<Riddle>(getApiResponse.Data.ToString(), MicrosoftDateFormatSettings);
                url = BaseUrl + @"/riddles/" + (RiddleCount + 10);
                var dbRiddleJson = JsonConvert.SerializeObject(riddle, MicrosoftDateFormatSettings); // re-serialize riddle and update the db object, no changes were made
                var content = new StringContent(dbRiddleJson, Encoding.UTF8, "application/json");
                var putResponse = httpClient.PutAsync(url, content);
                var result = putResponse.Result.Content.ReadAsStringAsync();
                var putApiResponse =
                    JsonConvert.DeserializeObject<ApiResponse>(result.Result, MicrosoftDateFormatSettings);
                return putApiResponse.StatusCode == (int) HttpStatusCode.NotFound;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_DeleteRiddle()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + TestRiddleId;
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.DeleteAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var deleteApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                return deleteApiResponse.StatusCode == (int) HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_DeleteRiddle_BadId()
        {
            try
            {
                const string url = BaseUrl + @"/riddles/crap";
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.DeleteAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var deleteApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                return deleteApiResponse.StatusCode == (int)HttpStatusCode.BadRequest;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
        public static bool Test_DeleteRiddle_IdNotFound()
        {
            try
            {
                var url = BaseUrl + @"/riddles/" + (RiddleCount + 10);
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);
                var response = httpClient.DeleteAsync(url);
                var bytes = response.Result.Content.ReadAsByteArrayAsync();
                var jsonResult = Encoding.UTF8.GetString(bytes.Result);
                var deleteApiResponse = JsonConvert.DeserializeObject<ApiResponse>(jsonResult, MicrosoftDateFormatSettings);
                return deleteApiResponse.StatusCode == (int)HttpStatusCode.NotFound;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
    }
}
