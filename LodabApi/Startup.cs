﻿using LodabApi.Data;
using LodabApi.Data.SeedData;
using LodabApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using System.IO;

namespace LodabApi
{
	public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
	        services.AddTransient<ApiContext, ApiContext>();
			services.AddTransient<RiddleService, RiddleService>();
			services.AddEntityFrameworkMySql();
	        services.AddMvc().AddJsonOptions(op => {
		        op.SerializerSettings.Converters.Add(new StringEnumConverter());
	        });
			var configBuilder = new ConfigurationBuilder()
		        .SetBasePath(Directory.GetCurrentDirectory())
		        .AddJsonFile("appsettings.json", optional: true);
	        var config = configBuilder.Build();
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
	        app.UseMvc().SeedData();
        }
    }
}
