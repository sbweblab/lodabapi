﻿using System.IO;
using LodabApi.Models.ContentModels;

namespace LodabApi.Data.SeedData.Content
{
	public class ContentData
    {
	    private readonly ApiContext _context;

	    public ContentData(ApiContext context)
	    {
		    _context = context;
	    }

	    public void SeedData()
	    {
		    var models = new JsonReader<ContentContainer>();
		    var path = $"{Directory.GetCurrentDirectory()}\\Data\\SeedData\\Content\\ContentData1.json";
		    _context.ContentContainer.AddRange(models.GetJsonFile(path));
		    path = $"{Directory.GetCurrentDirectory()}\\Data\\SeedData\\Content\\ContentData2.json";
		    _context.ContentContainer.AddRange(models.GetJsonFile(path));
		    _context.SaveChanges();
	    }
    }
}
