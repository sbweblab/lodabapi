﻿using System;

namespace LodabApi.Models
{
	public class SuspendableModel : BaseModel
    {
	    public bool IsActive { get; set; }
	    public DateTime? SuspendDate { get; set; }

	    public SuspendableModel()
	    {
		    IsActive = true;
	    }
	}
}
