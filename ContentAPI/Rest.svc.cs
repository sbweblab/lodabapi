﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;

namespace RiddlesAPI
{
    public class Rest : IRest
    {
        /// <summary>
        /// Retrieves all riddles
        /// </summary>
        /// <returns></returns>
        public ApiResponse GetRiddles()
        {
            var response = new ApiResponse();
            var db = new RiddlesDbDataContext();
            try
            {
                var riddles = db.Riddles.ToList();
                if (riddles.Count == 0)
                {
                    response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    response.StatusMessage = "Not Found";
                    return response;
                }
                response.Data = JsonConvert.SerializeObject(riddles);
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "OK";
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("Sequence contains no elements")) return response;
                response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                response.StatusMessage = "Not Found";
            }
            return response;
        }

        /// <summary>
        /// Retrieves one Riddle
        /// </summary>
        /// <returns></returns>
        public ApiResponse GetRiddle(string id)
        {
            var response = new ApiResponse();
            var db = new RiddlesDbDataContext();
            try
            {
                var riddle = db.Riddles.First(x => x.IsActive && x.Id == Convert.ToInt32(id));
                response.Data = JsonConvert.SerializeObject(riddle);
                if (WebOperationContext.Current != null)
                    response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "OK";
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("Sequence contains no elements")) return response;
                response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                response.StatusMessage = "Not Found";
                return response;
            }
            return response;
        }

        /// <summary>
        /// Creates a new riddle from a POST web request to /riddles
        /// </summary>
        /// <param name="riddle"></param>
        /// <returns></returns>
        public ApiResponse CreateRiddle(Riddle riddle)
        {
            var response = new ApiResponse();
            var db = new RiddlesDbDataContext();
            if (string.IsNullOrWhiteSpace(riddle.Question) || string.IsNullOrWhiteSpace(riddle.Answer)) /* validate provided data */
            {
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = WebOperationContext.Current.OutgoingResponse.StatusCode + ", please provide both a question and an answer.";
                return response;
            }
            try
            {
                var timeSig = DateTime.Now; // datetime fields match on create
                riddle.Created = timeSig;
                riddle.Modified = timeSig;
                riddle.SuspendDate = timeSig;
                db.Riddles.InsertOnSubmit(riddle);
                db.SubmitChanges();
                response.Data = JsonConvert.SerializeObject(riddle);
                if (WebOperationContext.Current != null)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Created;
                    response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                    response.StatusMessage = "Riddle created successfully.";
                    WebOperationContext.Current.OutgoingResponse.Headers["Location"] = @"/riddles/" + riddle.Id;
                }
            }
            catch (Exception e)
            {
                if (WebOperationContext.Current != null)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                }
                response.StatusMessage = "An exception was thrown: " + e.Message;
            }
            return response;
        }

        /// <summary>
        /// Updates a riddle with new information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newRiddle"></param>
        /// <returns></returns>
        public ApiResponse UpdateRiddle(string id, Riddle newRiddle)
        {
            var response = new ApiResponse();
            var db = new RiddlesDbDataContext();
            if (!int.TryParse(id, out var riddleId)) // convert the id string
            {
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = WebOperationContext.Current.OutgoingResponse.StatusCode + ", please provide a valid Riddle Id.";
                WebOperationContext.Current.OutgoingResponse.Headers["Location"] = @"/riddles/" + id;
                return response;
            }
            if (!db.Riddles.Any(x => x.Id == riddleId)) // 404 Not found if ID not found
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound("Riddle Id not found.");
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = WebOperationContext.Current.OutgoingResponse.StatusCode + ", please provide a valid Riddle Id.";
                return response;
            }
            var riddle = db.Riddles.First(x => x.Id == riddleId); // retrieve riddle from db
            var change = false;
            if (riddle.Question != newRiddle.Question)
            {
                riddle.Question = newRiddle.Question;
                change = true;
            }
            if (riddle.Answer != newRiddle.Answer)
            {
                riddle.Answer = newRiddle.Answer;
                change = true;
            }
            if (riddle.Hint != newRiddle.Hint)
            {
                riddle.Hint = newRiddle.Hint;
                change = true;
            }
            if (riddle.HonorableMention != newRiddle.HonorableMention)
            {
                riddle.HonorableMention = newRiddle.HonorableMention;
                change = true;
            }
            if (newRiddle.IsActive != riddle.IsActive)
            {
                riddle.IsActive = newRiddle.IsActive;
                change = true;
            }
            if (!change)
            {
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                response.Data = JsonConvert.SerializeObject(riddle);
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "No changes.";
                return response;
            }
            try
            {
                db.SubmitChanges();
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                response.Data = JsonConvert.SerializeObject(riddle);
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "Riddle updated successfully.";
            }
            catch (Exception e)
            {
                Debug.Assert(WebOperationContext.Current != null, "WebOperationContext.Current != null");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "An exception was thrown: " + e.Message;
            }
            return response;
        }

        /// <summary>
        /// Deletes a riddle.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApiResponse DeleteRiddle(string id)
        {
            var response = new ApiResponse();
            var db = new RiddlesDbDataContext();
            if (!int.TryParse(id, out var riddleId)) // convert the id string
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = WebOperationContext.Current.OutgoingResponse.StatusCode + ", please provide a valid Riddle Id.";
                WebOperationContext.Current.OutgoingResponse.Headers["Location"] = @"/riddles/" + id;
                return response;
            }
            if (!db.Riddles.Any(x => x.Id == riddleId)) // 404 Not found if ID not found
            {

                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound("Riddle Id not found.");
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = WebOperationContext.Current.OutgoingResponse.StatusCode + ", please provide a valid Riddle Id.";
                return response;
            }
            try
            {
                var riddle = db.Riddles.First(x => x.Id == riddleId); // retrieve riddle from db
                db.Riddles.DeleteOnSubmit(riddle);
                db.SubmitChanges();
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "Riddle deleted successfully.";
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                response.StatusCode = Convert.ToInt32(WebOperationContext.Current.OutgoingResponse.StatusCode);
                response.StatusMessage = "An exception was thrown: " + e.Message;
            }
            return response;
        }
    }
}
