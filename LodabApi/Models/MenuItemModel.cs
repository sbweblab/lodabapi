﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LodabApi.Models
{
    public class MenuItemModel : BaseModel
    {
	    public PageModel Page { get; set; }
	    public string Url { get; set; }
    }
}
