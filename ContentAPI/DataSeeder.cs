﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;

namespace RiddlesAPI
{
    public static class DataSeeder
    {
        public static void SeedData(RiddlesDbDataContext context)
        {
            if (context.Riddles.Any()) return;
            var filePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, @"Riddles.json");
            var riddles = JsonConvert.DeserializeObject<List<Riddle>>(File.ReadAllText(filePath));
            foreach (var r in riddles)
            {
                var newRiddle = new Riddle
                {
                    Question = r.Question,
                    Answer = r.Answer,
                    Hint = r.Hint,
                    HonorableMention = r.HonorableMention,
                    IsActive = true,
                    SuspendDate = DateTime.Now.Subtract(new TimeSpan(5000, 0, 0, 0)),
                    Modified = DateTime.Now,
                    Created = DateTime.Now
                };
                context.Riddles.InsertOnSubmit(newRiddle);
            }
            context.SubmitChanges();
        }
    }
}