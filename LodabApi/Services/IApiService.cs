﻿using LodabApi.Models;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace LodabApi.Services
{
	public interface IApiService
    {
	    Task<JObject> Search(string searchCriteria, string page, string count);
	    Task<JObject> Search(string searchCriteria, string stringLocation, string page, string count);
	    Task<JObject> GetCollection(string page, string count);
	    Task<JObject> GetItem(int id);
	    Task<JObject> PutItem<T>(int id, T incomingModel) where T : BaseModel;
	    Task<JObject> PostItem();
	    Task<JObject> DeleteItem();
    }
}
