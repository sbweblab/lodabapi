﻿namespace LodabApi.Models
{
	public class BaseModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int SiteId { get; set; }
	}
}
