﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LodabApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ContentContainer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    SuspendDate = table.Column<DateTime>(nullable: true),
                    SiteId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Template = table.Column<int>(nullable: false),
                    MenuTitle = table.Column<string>(nullable: true),
                    ContentTarget = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentContainer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Riddle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    SuspendDate = table.Column<DateTime>(nullable: true),
                    Question = table.Column<string>(nullable: false),
                    Answer = table.Column<string>(nullable: false),
                    Hint = table.Column<string>(nullable: true),
                    HonorableMentions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Riddle", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Site",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    SuspendDate = table.Column<DateTime>(nullable: true),
                    SiteName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Site", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContentGrouping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<string>(nullable: true),
                    Template = table.Column<int>(nullable: false),
                    ContentContainerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentGrouping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentGrouping_ContentContainer_ContentContainerId",
                        column: x => x.ContentContainerId,
                        principalTable: "ContentContainer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    MenuName = table.Column<string>(nullable: true),
                    MainImageId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    ContentGroupingId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentItem_ContentGrouping_ContentGroupingId",
                        column: x => x.ContentGroupingId,
                        principalTable: "ContentGrouping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    ContentItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentDetail_ContentItem_ContentItemId",
                        column: x => x.ContentItemId,
                        principalTable: "ContentItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentImage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    ContentItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentImage_ContentItem_ContentItemId",
                        column: x => x.ContentItemId,
                        principalTable: "ContentItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContentDetail_ContentItemId",
                table: "ContentDetail",
                column: "ContentItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentGrouping_ContentContainerId",
                table: "ContentGrouping",
                column: "ContentContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentImage_ContentItemId",
                table: "ContentImage",
                column: "ContentItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentItem_ContentGroupingId",
                table: "ContentItem",
                column: "ContentGroupingId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentItem_MainImageId",
                table: "ContentItem",
                column: "MainImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContentItem_ContentImage_MainImageId",
                table: "ContentItem",
                column: "MainImageId",
                principalTable: "ContentImage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContentImage_ContentItem_ContentItemId",
                table: "ContentImage");

            migrationBuilder.DropTable(
                name: "ContentDetail");

            migrationBuilder.DropTable(
                name: "Riddle");

            migrationBuilder.DropTable(
                name: "Site");

            migrationBuilder.DropTable(
                name: "ContentItem");

            migrationBuilder.DropTable(
                name: "ContentGrouping");

            migrationBuilder.DropTable(
                name: "ContentImage");

            migrationBuilder.DropTable(
                name: "ContentContainer");
        }
    }
}
