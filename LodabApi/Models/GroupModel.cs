﻿using System.Collections.Generic;

namespace LodabApi.Models
{
	public class GroupModel : BaseModel
	{
		public string Template { get; set; }
		public List<ItemModel> ContentItems { get; set; }

		public GroupModel()
		{
			ContentItems = new List<ItemModel>();
		}
	}
}
