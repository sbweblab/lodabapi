﻿using System.Collections.Generic;
using LodabApi.Models;

namespace LodabApi.Data.SeedData.Site
{
	public class SiteData
	{
		private readonly ApiContext _context;

		public SiteData(ApiContext context)
		{
			_context = context;
		}

		public void SeedData()
		{
			var sites = new List<SiteModel>
			{
				new SiteModel{Id = 1, IsActive = true, SiteName = "LumberGames"},
				new SiteModel{Id = 2, IsActive = true, SiteName = "Riddles"}
			};
			_context.Site.AddRange(sites);
			_context.SaveChanges();
		}
	}
}
