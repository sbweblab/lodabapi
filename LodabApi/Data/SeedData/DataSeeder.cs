﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using LodabApi.Data.SeedData.Content;
using LodabApi.Data.SeedData.Riddles;
using LodabApi.Data.SeedData.Site;

namespace LodabApi.Data.SeedData
{
	public static class DataSeeder
    {
	    public static void SeedData(this IApplicationBuilder app)
	    {
		    var context = app.ApplicationServices.GetService<ApiContext>();
		    context.Database.Migrate();
		    if (!context.Site.Any()) new SiteData(context).SeedData();
		    if (!context.Riddle.Any()) new RiddlesData(context).SeedData();
		    if (!context.ContentContainer.Any()) new ContentData(context).SeedData();
	    }
	}
}
