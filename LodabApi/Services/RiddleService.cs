﻿using LodabApi.Data;
using LodabApi.Data.Pagination;
using LodabApi.Models;
using LodabApi.Models.RiddleModels;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LodabApi.Services
{
	public class RiddleService : IApiService
	{
		private readonly ApiContext _context;
		public RiddleService(ApiContext context)
		{
			_context = context;
		}
		public Task<JObject> Search(string searchCriteria, string page, string count)
		{
			throw new NotImplementedException();
		}
		/// <summary>
		/// Returns Paginated Results where the Question, Answer, Hint, or HonorableMentions includes the string for Search Criteria.
		/// </summary>
		/// <param name="searchCriteria"></param>
		/// <param name="stringLocation">'Question' will search the Question, 'Answer' will search the Answer, 'Hint' will search the Hint, 'HonorableMentions' will search the Honorable Mentions, all other values will search all fields.</param>
		/// <param name="page"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public Task<JObject> Search(string searchCriteria, string stringLocation, string page, string count)
		{
			int p = int.TryParse(page, out p) ? p : 1;
			int c = int.TryParse(count, out c) ? c : 10;
			IQueryable<RiddleModel> results = _context.Riddle;

			switch (stringLocation.ToLower())
			{
				case "question":
					results = results.Where(x => x.Question.Contains(searchCriteria));
					break;
				case "answer":
					results = results.Where(x => x.Answer.Contains(searchCriteria));
					break;
				case "hint":
					results = results.Where(x => x.Hint.Contains(searchCriteria));
					break;
				case "honorablementions":
					results = results.Where(x => x.HonorableMentions.Contains(searchCriteria));
					break;
				default:
					results = results.Where(x => x.Question.Contains(searchCriteria)
										|| x.Answer.Contains(searchCriteria)
										|| x.Hint.Contains(searchCriteria)
										|| x.HonorableMentions.Contains(searchCriteria));
					break;
			}
			return Task.FromResult(JObject.FromObject(results.OrderBy(x=>x.Id).GetPaged(p, c)));
		}

		public Task<JObject> GetCollection(string page, string count)
		{
			int p = int.TryParse(page, out p) ? p : 1;
			int c = int.TryParse(count, out c) ? c : 10;
			return Task.FromResult(
				JObject.FromObject(_context.Riddle.OrderBy(x => x.Id).GetPaged(p, c)));
		}

		public Task<JObject> GetItem(int id)
		{
			return Task.FromResult(
				JObject.FromObject(_context.Riddle.FirstOrDefault(x => x.Id == id)));
		}

		//public Task<JObject> PutItem<T>(int id, T incomingModel)
		//{
		//	throw new NotImplementedException();
		//}

		public async Task<JObject> PutItem<T>(int id, T incomingModel) where T : BaseModel
		{
			try
			{
				incomingModel.Id = id;
				_context.Update(incomingModel);
				await _context.SaveChangesAsync();
				return JObject.Parse("{success:true}");
			}
			catch //(Exception x)
			{
				return null;
			}
		}

		public Task<JObject> PostItem()
		{
			throw new NotImplementedException();
		}

		public Task<JObject> DeleteItem()
		{
			throw new NotImplementedException();
		}
	}
}
