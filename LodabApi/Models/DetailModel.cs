﻿namespace LodabApi.Models
{
	public class DetailModel : BaseModel
	{
		public string Title { get; set; }
		public string Data { get; set; }
		public int Order { get; set; }
	}
}
