﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mime;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.ApplicationServices;
using System.Web.Routing;

namespace RiddlesAPI
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.Add(new ServiceRoute("api", new WebServiceHostFactory(), typeof(Rest)));
            var context = new RiddlesDbDataContext();
            if (context.DatabaseExists()) return;
            context.CreateDatabase();
            context.SubmitChanges();
            DataSeeder.SeedData(context);
            context.Dispose();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Response.Write("<font face=\"Tahoma\" size=\"2\" color=\"red\">");
            Response.Write("FAIL!!<hr></font>");
            Response.Write("<font face=\"Arial\" size=\"2\">");
            Response.Write(Server.GetLastError().Message.ToString());
            Response.Write("<hr>" + Server.GetLastError().ToString());
            Server.ClearError();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}