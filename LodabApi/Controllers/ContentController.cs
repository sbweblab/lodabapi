﻿using LodabApi.Data;
using LodabApi.Models.RiddleModels;
using LodabApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LodabApi.Controllers
{
	[Route("[controller]")]
    public class ContentController : Controller
    {
	    private readonly ApiContext _context;
	    private readonly IApiService _service;

	    public ContentController(ApiContext context, IServiceProvider provider) 
	    {
			_service = provider.GetRequiredService<RiddleService>();
			_context = context;
		}
		
	    [HttpGet]
	    public ActionResult NoPathReturnsOk()
	    {
		    return Ok("You shall not pass!");
	    }
		
		[HttpGet("Container/{id}")]
	    public ActionResult Get(int id,
			[FromQuery] int site = 0,
	        [FromQuery] int page = 1, 
	        [FromQuery] int count = 10)
        {
	        if (!SiteExists(site)) return BadRequest("Site Not Found");
	        if (!ContainerExists(site)) return BadRequest($"Container/{id} Not Found"); ;
			return Json(_context.ContentContainer
		        .Include(x => x.DisplayItems)
				.ThenInclude(y => y.ContentItems)
		        .ThenInclude(z => z.SubImages)
				.Include(x => x.DisplayItems)
				.ThenInclude(y => y.ContentItems)
				.ThenInclude(z => z.Details)
				.Include(x => x.DisplayItems)
				.ThenInclude(y => y.ContentItems)
				.ThenInclude(z => z.MainImage)
				.FirstOrDefault(x => x.Id == id 
					&& x.SuspendDate == null
					&& x.SiteId == site
				));
        }

	    [HttpGet("Item/{id}")]
	    public ActionResult GetContentItem(int id,
	    [FromQuery] int site = 0,
	    [FromQuery] int page = 1,
	    [FromQuery] int count = 10)
	    {
		    if (!SiteExists(site)) return Ok();
		    if (!ItemExists(id)) return Ok();
		    return Json(_context.ContentItem
				    .Include(z => z.SubImages)
				    .Include(z => z.Details)
				    .FirstOrDefault(x => x.Id == id)
		    );
		}
		
		[HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]RiddleModel value)
        {
	        if (!ModelState.IsValid) return BadRequest(ModelState);

	        var x = await _service.PutItem(id, value);

	        return Ok(x);
        }

	    public bool SiteExists(int siteId)
	    {
		    return _context.Site.Any(x => x.Id == siteId && x.SuspendDate == null);
	    }

	    public bool ItemExists(int itemId)
	    {
		    return _context.ContentItem.Any(x => x.Id == itemId);
	    }

	    public bool ContainerExists(int containerId)
	    {
		    return _context.ContentContainer.Any(x => x.Id == containerId);
	    }
    }
}
