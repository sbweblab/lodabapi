﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RiddlesAPI
{
    [ServiceContract]
    public interface IRest
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "riddles")]
        ApiResponse GetRiddles();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "riddles/{id}")]
        ApiResponse GetRiddle(string id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "riddles")]
        ApiResponse CreateRiddle(Riddle riddle);

        [OperationContract]
        [WebInvoke(Method = "PUT",
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "riddles/{id}")]
        ApiResponse UpdateRiddle(string id, Riddle newRiddle);

        [OperationContract]
        [WebInvoke(Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "riddles/{id}")]
        ApiResponse DeleteRiddle(string id);
    }
    
    [DataContract]
    public class ApiResponse
    {
        [DataMember]
        public int StatusCode;

        [DataMember]
        public string StatusMessage;

        [DataMember]
        public object Data;
    }
}
