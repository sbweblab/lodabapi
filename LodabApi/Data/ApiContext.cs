﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using LodabApi.Models;
using LodabApi.Models.ContentModels;
using LodabApi.Models.RiddleModels;

namespace LodabApi.Data
{
	public class ApiContext : DbContext
    {
	    public DbSet<RiddleModel> Riddle { get; set; }
	    public DbSet<ContentContainer> ContentContainer { get; set; }
	    public DbSet<SiteModel> Site { get; set; }
	    public DbSet<ItemModel> ContentItem { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();
			optionsBuilder.UseMySql(configuration["ConnectionString"]);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
	    {
		    base.OnModelCreating(modelBuilder);

		    modelBuilder.Entity<RiddleModel>(entity =>
		    {
			    entity.HasKey(e => e.Id);
			    entity.Property(e => e.Question).IsRequired();
			    entity.Property(e => e.Answer).IsRequired();
		    });

		    modelBuilder.Entity<ContentContainer>(entity =>
		    {
			    entity.HasKey(e => e.Id);
		    });
		    modelBuilder.Entity<DetailModel>();
		    modelBuilder.Entity<GroupModel>();
		    modelBuilder.Entity<ImageModel>();
		    modelBuilder.Entity<ItemModel>();

			modelBuilder.Entity<SiteModel>(entity =>
		    {
			    entity.HasKey(e => e.Id);
		    });
		}
	}
}
